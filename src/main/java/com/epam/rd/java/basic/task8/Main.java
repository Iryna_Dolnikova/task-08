package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.util.Sorting;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}

		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////

		// get container
		DOMController domController = new DOMController(xmlFileName);
		domController.parse(true);
		Flowers flowers = domController.getFlowers();

		// sort (case 1)
		Sorting.setSortFlowersByAveLen(flowers);

		// save
		String outputXmlFile = "output.dom.xml";
		DOMController.saveXML(flowers, outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////

		// get
		SAXController saxController = new SAXController(xmlFileName);
		saxController.parse(true);
		flowers = saxController.getFlowers();

		// sort  (case 2)
		Sorting.setSortFlowersByFlowersName(flowers);

		// save
		outputXmlFile = "output.sax.xml";
		DOMController.saveXML(flowers, outputXmlFile);

		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////

		// get
		STAXController staxController = new STAXController(xmlFileName);
		staxController.parse();
		flowers = staxController.getFlowers();

		// sort  (case 3)
		Sorting.setSortFlowersByWateringMeasure(flowers);

		// save
		outputXmlFile = "output.stax.xml";
		DOMController.saveXML(flowers, outputXmlFile);
	}

}
