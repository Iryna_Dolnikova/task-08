package com.epam.rd.java.basic.task8.util;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flowers;

import java.util.Comparator;

public class Sorting {

    private Sorting() {
        //Blocked creating elements
    }

    public static void setSortFlowersByFlowersName(Flowers flowers) {
        flowers.getFlower().sort(Comparator.comparing(Flower::getName));
    }

    public static void setSortFlowersByAveLen(Flowers flowers) {
        flowers.getFlower().sort(Comparator.comparing(o -> o.getVisualParameters().getAveLenFlower().getValue()));
    }

    public static void setSortFlowersByWateringMeasure(Flowers flowers) {
        flowers.getFlower().sort(Comparator.comparing(o -> o.getGrowingTips().getWatering().getValue()));
    }

}
