package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.constant.XMLConstants;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import org.xml.sax.helpers.DefaultHandler;

import java.math.BigInteger;
import java.util.Objects;

public abstract class DefaultHandlerController extends DefaultHandler {

    protected final String xmlFileName;

    protected String currentElement;
    protected Flowers flowers;
    protected Flower flower;
    protected GrowingTips growingTips;
    protected VisualParameters visualParameters;

    public DefaultHandlerController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public Flowers getFlowers() {
        return flowers;
    }

    public void endTags(String localName) {
        if (Objects.equals(localName, XMLConstants.FLOWER.value())) {
            flowers.getFlower().add(flower);
        } else if (Objects.equals(localName, XMLConstants.VISUAL_PARAMETERS.value())) {
            flower.setVisualParameters(visualParameters);
        } else if (Objects.equals(localName, XMLConstants.GROWING_TIPS.value())) {
            flower.setGrowingTips(growingTips);
        }
    }

    protected void extractStartElement() {
        if (Objects.equals(currentElement, XMLConstants.FLOWERS.value())) {
            flowers = new Flowers();
        } else if (Objects.equals(currentElement, XMLConstants.FLOWER.value())) {
            flower = new Flower();
        } else if (Objects.equals(currentElement, XMLConstants.VISUAL_PARAMETERS.value())) {
            visualParameters = new VisualParameters();
        } else if (Objects.equals(currentElement, XMLConstants.GROWING_TIPS.value())) {
            growingTips = new GrowingTips();
        }
    }

    protected void fillFlower(String elementText) {
        if (Objects.equals(currentElement, XMLConstants.NAME.value())) {
            flower.setName(elementText);
        } else if (Objects.equals(currentElement, XMLConstants.SOIL.value())) {
            flower.setSoil(elementText);
        } else if (Objects.equals(currentElement, XMLConstants.ORIGIN.value())) {
            flower.setOrigin(elementText);
        } else if (Objects.equals(currentElement, XMLConstants.STEAM_COLOR.value())) {
            visualParameters.setStemColour(elementText);
        } else if (Objects.equals(currentElement, XMLConstants.LEAF_COLOR.value())) {
            visualParameters.setLeafColour(elementText);
        } else if (Objects.equals(currentElement, XMLConstants.AVE_LEN_FLOWER.value())) {
            visualParameters.getAveLenFlower().setValue(BigInteger.valueOf(Integer.parseInt(elementText)));
        } else if (Objects.equals(currentElement, XMLConstants.TEMPERATURE.value())) {
            growingTips.getTemperature().setValue(BigInteger.valueOf(Integer.parseInt(elementText)));
        } else if (Objects.equals(currentElement, XMLConstants.WATERING.value())) {
            growingTips.getWatering().setValue(BigInteger.valueOf(Integer.parseInt(elementText)));
        } else if (Objects.equals(currentElement, XMLConstants.MULTIPLYING.value())) {
            flower.setMultiplying(elementText);
        }
    }

}
