package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constant.Constants;
import com.epam.rd.java.basic.task8.constant.XMLConstants;
import com.epam.rd.java.basic.task8.entity.AveLenFlower;
import com.epam.rd.java.basic.task8.entity.Lighting;
import com.epam.rd.java.basic.task8.entity.Temperature;
import com.epam.rd.java.basic.task8.entity.Watering;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.Objects;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandlerController {

    public SAXController(String xmlFileName) {
        super(xmlFileName);
    }

    public void parse(boolean validate) throws SAXException,
            ParserConfigurationException, IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance(
                Constants.CLASS_SAX_PARSER_FACTORY_INTERNAL,
                this.getClass().getClassLoader());
        factory.setFeature(Constants.FEATURE_DISALLOW_DOCTYPE_ON, true);
        factory.setNamespaceAware(true);

        if (validate) {
            factory.setFeature(Constants.FEATURE_TURN_VALIDATION_ON, true);
            factory.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
        }

        javax.xml.parsers.SAXParser parser = factory.newSAXParser();
        parser.parse(xmlFileName, this);
    }


    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        currentElement = localName;
        extractStartElement();

        if (Objects.equals(currentElement, XMLConstants.AVE_LEN_FLOWER.value())) {
            visualParameters.setAveLenFlower(new AveLenFlower());
            if (attributes.getLength() > 0) {
                visualParameters.getAveLenFlower().setMeasure(attributes.getValue(uri,
                        XMLConstants.MEASURE.value()));
            }
        } else {
            checkGrowingTips(uri, attributes);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        endTags(localName);
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        String elementText = new String(ch, start, length).trim();
        if (elementText.isEmpty()) {
            return;
        }

        fillFlower(elementText);
    }

    public void checkGrowingTips(String uri, Attributes attributes) {
        if (Objects.equals(currentElement, XMLConstants.TEMPERATURE.value())) {
            growingTips.setTemperature(new Temperature());
            if (attributes.getLength() > 0) {
                growingTips.getTemperature().setMeasure(attributes.getValue(uri,
                        XMLConstants.MEASURE.value()));
            }
        } else if (Objects.equals(currentElement, XMLConstants.LIGHTNING.value())) {
            growingTips.setLighting(new Lighting());
            if (attributes.getLength() > 0) {
                growingTips.getLighting().setLightRequiring(attributes.getValue(0));
            }
        } else if (Objects.equals(currentElement, XMLConstants.WATERING.value())) {
            growingTips.setWatering(new Watering());
            if (attributes.getLength() > 0) {
                growingTips.getWatering().setMeasure(attributes.getValue(uri,
                        XMLConstants.WATERING.value()));
            }
        }
    }

}