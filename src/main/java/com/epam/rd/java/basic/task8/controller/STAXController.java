package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.AveLenFlower;
import com.epam.rd.java.basic.task8.constant.XMLConstants;
import com.epam.rd.java.basic.task8.entity.Lighting;
import com.epam.rd.java.basic.task8.entity.Temperature;
import com.epam.rd.java.basic.task8.entity.Watering;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import javax.xml.transform.stream.StreamSource;
import java.util.Objects;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandlerController {

    public STAXController(String xmlFileName) {
        super(xmlFileName);
    }

    public void parse() throws XMLStreamException {
        XMLInputFactory factory = XMLInputFactory.newInstance();
        factory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
        factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);

        XMLEventReader reader = factory.createXMLEventReader(new StreamSource(xmlFileName));

        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();
            if (event.isCharacters() && event.asCharacters().isWhiteSpace()) {
                continue;
            }

            if (event.isStartElement()) {
                StartElement startElement = event.asStartElement();
                currentElement = startElement.getName().getLocalPart();
                startTags(startElement);
            }

            if (event.isCharacters()) {
                Characters characters = event.asCharacters();
                contentHandler(characters);
            }

            if (event.isEndElement()) {
                EndElement endElement = event.asEndElement();
                String localName = endElement.getName().getLocalPart();
                endTags(localName);
            }
        }

        reader.close();
    }

    public void startTags(StartElement startElement) {
        extractStartElement();

        if (Objects.equals(currentElement, XMLConstants.AVE_LEN_FLOWER.value())) {
            visualParameters.setAveLenFlower(new AveLenFlower());
            Attribute attribute = startElement.getAttributeByName(
                    new QName(XMLConstants.MEASURE.value()));
            if (attribute != null) {
                visualParameters.getAveLenFlower().setMeasure(attribute.getValue());
            }
        } else {
            startTagsGrowingTips(startElement);
        }
    }

    public void startTagsGrowingTips(StartElement startElement) {
        if (Objects.equals(currentElement, XMLConstants.TEMPERATURE.value())) {
            growingTips.setTemperature(new Temperature());
            Attribute attribute = startElement.getAttributeByName(
                    new QName(XMLConstants.MEASURE.value()));
            if (attribute != null) {
                growingTips.getTemperature().setMeasure(attribute.getValue());
            }
        } else if (Objects.equals(currentElement, XMLConstants.LIGHTNING.value())) {
            growingTips.setLighting(new Lighting());
            Attribute attribute = startElement.getAttributeByName(
                    new QName(XMLConstants.LIGHT_REQUIRING.value()));
            if (attribute != null) {
                growingTips.getLighting().setLightRequiring(attribute.getValue());
            }
        } else if (Objects.equals(currentElement, XMLConstants.WATERING.value())) {
            growingTips.setWatering(new Watering());
            Attribute attribute = startElement.getAttributeByName(
                    new QName(XMLConstants.MEASURE.value()));
            if (attribute != null) {
                growingTips.getWatering().setMeasure(attribute.getValue());
            }
        }
    }

    public void contentHandler(Characters characters) {
        String elementText = characters.getData();
        fillFlower(elementText);
    }

}