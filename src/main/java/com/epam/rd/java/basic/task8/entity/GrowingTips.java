package com.epam.rd.java.basic.task8.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "tempreture",
        "lighting",
        "watering"
})
public class GrowingTips {

    @XmlElement(namespace = "http://www.nure.ua", required = true)
    protected Temperature temperature;
    @XmlElement(namespace = "http://www.nure.ua", required = true)
    protected Lighting lighting;
    @XmlElement(namespace = "http://www.nure.ua")
    protected Watering watering;

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature value) {
        this.temperature = value;
    }

    public Lighting getLighting() {
        return lighting;
    }

    public void setLighting(Lighting value) {
        this.lighting = value;
    }

    public Watering getWatering() {
        return watering;
    }

    public void setWatering(Watering value) {
        this.watering = value;
    }

}
