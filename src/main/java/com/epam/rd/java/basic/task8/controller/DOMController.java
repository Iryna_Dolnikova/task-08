package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constant.Constants;
import com.epam.rd.java.basic.task8.constant.XMLConstants;
import com.epam.rd.java.basic.task8.entity.*;
import com.epam.rd.java.basic.task8.util.Saving;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.math.BigInteger;

import static com.epam.rd.java.basic.task8.constant.XMLConstants.*;

/**
 * Controller for DOM parser.
 */
public class DOMController {

    private final String xmlFileName;
    private Flowers flowers;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public Flowers getFlowers() {
        return flowers;
    }

    public void parse(boolean validate) throws ParserConfigurationException,
            SAXException, IOException {

        DocumentBuilderFactory dbf = getDocumentBuilderFactory();
        if (validate) {
            dbf.setFeature(Constants.FEATURE_TURN_VALIDATION_ON, true);
            dbf.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
        }

        DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
        documentBuilder.setErrorHandler(new DefaultHandler() {
            @Override
            public void error(SAXParseException e) throws SAXException {
                throw e;
            }
        });

        Document document = documentBuilder.parse(xmlFileName);
        Element root = document.getDocumentElement();

        flowers = new Flowers();
        NodeList flowerNodes = root.getElementsByTagName(FLOWER.value());

        for (int j = 0; j < flowerNodes.getLength(); j++) {
            Flower flower = getFlower(flowerNodes.item(j));
            flowers.getFlower().add(flower);
        }
    }

    public static void saveXML(Flowers flowers, String xmlFileName)
            throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory documentBuilderFactory = getDocumentBuilderFactory();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.newDocument();

        Element element = document.createElementNS(ATTRIBUTE.value(), FLOWERS.value());
        document.appendChild(element);

        // add questions elements
        for (Flower flower : flowers.getFlower()) {
            // flower
            Element flowerElement = document.createElement(FLOWER.value());
            element.appendChild(flowerElement);
            createElement(document, flowerElement, NAME, flower.getName());
            createElement(document, flowerElement, SOIL, flower.getSoil());
            createElement(document, flowerElement, ORIGIN, flower.getOrigin());

            // visual parameters
            Element visualParametersElement = document.createElement(VISUAL_PARAMETERS.value());
            flowerElement.appendChild(visualParametersElement);
            createElement(document, visualParametersElement, STEAM_COLOR, flower.getVisualParameters().getStemColour());
            createElement(document, visualParametersElement, LEAF_COLOR, flower.getVisualParameters().getLeafColour());

            // aveLenFlower
            Element aveLenFlowerElement = document.createElement(AVE_LEN_FLOWER.value());
            aveLenFlowerElement.setTextContent(flower.getVisualParameters().getAveLenFlower().getValue().toString());
            aveLenFlowerElement.setAttribute(MEASURE.value(),
                    flower.getVisualParameters().getAveLenFlower().getMeasure());
            visualParametersElement.appendChild(aveLenFlowerElement);

            // growingTips
            Element growingTipsElement = document.createElement(GROWING_TIPS.value());
            flowerElement.appendChild(growingTipsElement);

            // temperature
            Element temperatureElement = document.createElement(TEMPERATURE.value());
            temperatureElement.setTextContent(flower.getGrowingTips().getTemperature().getValue().toString());
            temperatureElement.setAttribute(MEASURE.value(), flower.getGrowingTips().getTemperature().getMeasure());
            growingTipsElement.appendChild(temperatureElement);

            // lighting
            Element lightingElement = document.createElement(LIGHTNING.value());
            lightingElement.setAttribute(LIGHT_REQUIRING.value(),
                    flower.getGrowingTips().getLighting().getLightRequiring());
            growingTipsElement.appendChild(lightingElement);

            // watering
            Element wateringElement = document.createElement(WATERING.value());
            wateringElement.setTextContent(flower.getGrowingTips().getWatering().getValue().toString());
            wateringElement.setAttribute(MEASURE.value(), flower.getGrowingTips().getWatering().getMeasure());
            growingTipsElement.appendChild(wateringElement);

            // multiplying
            createElement(document, flowerElement, MULTIPLYING, flower.getMultiplying());
        }

        Saving.saveToXML(document, xmlFileName);
    }

    private static DocumentBuilderFactory getDocumentBuilderFactory() throws ParserConfigurationException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance(
                Constants.CLASS_DOCUMENT_BUILDER_FACTORY_INTERNAL,
                DOMController.class.getClassLoader());
        documentBuilderFactory.setFeature(Constants.FEATURE_DISALLOW_DOCTYPE_ON, true);
        documentBuilderFactory.setNamespaceAware(true);
        return documentBuilderFactory;
    }

    private static void createElement(Document document, Element element, XMLConstants xmlName, String value) {
        Element childElement = document.createElement(xmlName.value());
        childElement.setTextContent(value);
        element.appendChild(childElement);
    }

    private Flower getFlower(Node node) {
        Flower flower = new Flower();
        Element flowerElement = (Element) node;

        node = flowerElement.getElementsByTagName(NAME.value()).item(0);
        flower.setName(node.getTextContent());

        node = flowerElement.getElementsByTagName(SOIL.value()).item(0);
        flower.setSoil(node.getTextContent());

        node = flowerElement.getElementsByTagName(ORIGIN.value()).item(0);
        flower.setOrigin(node.getTextContent());

        flower.setVisualParameters(getVisualParameters(node));
        flower.setGrowingTips(getGrowingTips(node));

        node = flowerElement.getElementsByTagName(MULTIPLYING.value()).item(0);
        flower.setMultiplying(node.getTextContent());

        return flower;
    }

    private VisualParameters getVisualParameters(Node node) {
        VisualParameters visualParameters = new VisualParameters();
        Element visualParametersElement = (Element) node;

        node = visualParametersElement.getElementsByTagName(STEAM_COLOR.value()).item(0);
        visualParameters.setStemColour(node.getTextContent());

        node = visualParametersElement.getElementsByTagName(LEAF_COLOR.value()).item(0);
        visualParameters.setLeafColour(node.getTextContent());

        visualParameters.setAveLenFlower(getAveLenFlower(node));
        return visualParameters;
    }

    private AveLenFlower getAveLenFlower(Node node) {
        AveLenFlower aveLenFlower = new AveLenFlower();
        Element aveLenFlowerElement = (Element) node;

        node = aveLenFlowerElement.getElementsByTagName(AVE_LEN_FLOWER.value()).item(0);
        aveLenFlower.setValue(BigInteger.valueOf(Integer.parseInt(node.getTextContent())));
        aveLenFlower.setMeasure(node.getAttributes().item(0).getTextContent());
        return aveLenFlower;
    }

    private GrowingTips getGrowingTips(Node node) {
        GrowingTips growingTips = new GrowingTips();
        Element growingTipsElement = (Element) node;

        node = growingTipsElement.getElementsByTagName(TEMPERATURE.value()).item(0);
        growingTips.setTemperature(getTemperature(node));

        node = growingTipsElement.getElementsByTagName(LIGHTNING.value()).item(0);
        growingTips.setLighting(getLighting(node));

        node = growingTipsElement.getElementsByTagName(WATERING.value()).item(0);
        growingTips.setWatering(getWatering(node));

        return growingTips;
    }

    private Temperature getTemperature(Node node) {
        Temperature temperature = new Temperature();
        temperature.setValue(BigInteger.valueOf(Integer.parseInt(node.getTextContent())));
        temperature.setMeasure(node.getAttributes().item(0).getTextContent());
        return temperature;
    }

    private Watering getWatering(Node node) {
        Watering watering = new Watering();
        watering.setValue(BigInteger.valueOf(Integer.parseInt(node.getTextContent())));
        watering.setMeasure(node.getAttributes().item(0).getTextContent());
        return watering;
    }

    private Lighting getLighting(Node node) {
        Lighting lighting = new Lighting();
        lighting.setLightRequiring(node.getAttributes().item(0).getTextContent());
        return lighting;
    }

}
