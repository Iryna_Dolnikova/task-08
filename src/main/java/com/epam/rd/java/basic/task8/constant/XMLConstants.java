package com.epam.rd.java.basic.task8.constant;

public enum XMLConstants {
    ATTRIBUTE("http://www.nure.ua"), 

    FLOWERS("flowers"), FLOWER("flower"),
    NAME("name"), SOIL("soil"), ORIGIN("origin"),
    VISUAL_PARAMETERS("visualParameters"), STEAM_COLOR("stemColour"), LEAF_COLOR("leafColour"),
    AVE_LEN_FLOWER("aveLenFlower"),

    GROWING_TIPS("growingTips"), TEMPERATURE("temperature"),
    LIGHTNING("lighting"), WATERING("watering"), MULTIPLYING("multiplying"),
    MEASURE("measure"), LIGHT_REQUIRING("lightRequiring");

    private final String value;

    public String value() {
        return value;
    }

    XMLConstants(String value) {
        this.value = value.intern();
    }

}
